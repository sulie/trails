var map = L.map('map');

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '© OpenStreetMap contributors'
}).addTo(map);


$.ajax ( {

    dataType: "json",
    url: "http://api.ipstack.com/81.47.179.37?access_key=001d98322ea825dacda1b1a45357796a&format=1",

    success: function(result){

        console.log(result);

            L.Routing.control({
                waypoints: [
                  L.latLng(result.latitude,result.longitude),
                  L.latLng(28.5, -13.9)
                ],
                routeWhileDragging: true
            }).addTo(map);
    }

});